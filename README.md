![Pipeline Status](https://gitlab.com/a9692/hvm/experimentos/badges/master/pipeline.svg)

Página web para los experimentos "hacelo vos mismo"

Podes ver la última página generada aquí: https://a9692.gitlab.io/hvm/experimentos/

## Generá el sitio localmente

Para generar el sitio en tu máquina local necesitas instalar [Hugo](https://gohugo.io/getting-started/installing/).

Luego de clonar este repositorio, actualizá el submódulo con el theme:

    git submodule update --init --recursive

Ahora tenes todo lo necesario para generar el sitio. Lo podes hacer con el comando

    hugo server --disableFastRender

Los cambios de contenido se regeneran automaticamente.
Si haces cambios al CSS acordate the hacer una recarga profunda de tu explorador,
e.g. en Firefox Ctrl+F5.




